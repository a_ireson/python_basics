# Variables are used to store values in the computers memory
# There are a number of different types of variables:
#     float: e.g. 1.223 this is a real number
#     int: e.g 4 this is an integer
#     str: e.g. "Andrew" this is a string

mass=4.2

print(mass)

myname='Andrew'
print(myname)

print(str(mass)+" " + myname)

# To convert a float to a string, use the function str()

number_of_rabbits=3

print(number_of_rabbits)

print(type(number_of_rabbits))
print(type(mass))
print(type(myname))

print(mass*number_of_rabbits)

print(mass/number_of_rabbits)