T=[-10,-5,0,5,10,15,20]

for Ti in T:
    print('The temperature is currently %d degrees C'%Ti)
    
for Ti in T:
    if Ti > 0:
        print('It is warm... it is %d deg C'%Ti)
    elif Ti == 0:
        print('Borderline... %d deg C'%Ti)
    else:
        print('It is cold... it is %d deg C'%Ti)
