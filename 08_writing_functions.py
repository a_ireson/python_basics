def mean(mylist):
    # A simple function to calculate the mean.
    average=float(sum(mylist))/len(mylist)
    return average

x=[1,2,3,4,5,6,7,8,9,10]
# average=float(sum(x))/len(x)
# print(average)

print(mean(x))

y=[2,4,6,8,10,12,14,16,18,20]
print(mean(y))